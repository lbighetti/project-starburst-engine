# Project Starburst - Engine

Be a rockstar, today!

## Technologies used in the Backend

* Kotlin
* Gradle
* Spring Boot
    * Spring Data / Hibernate
    * Spring Actuator
* Heroku
* Postgres
* JSONDoc

Current deployed at: http://project-starburst-engine.herokuapp.com/