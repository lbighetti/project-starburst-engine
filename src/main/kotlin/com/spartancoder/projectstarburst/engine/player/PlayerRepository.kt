package com.spartancoder.projectstarburst.engine.player

import org.springframework.data.mongodb.repository.MongoRepository

interface PlayerRepository : MongoRepository<Player, String> {
    fun findByEmail(email: String): List<Player>
}