package com.spartancoder.projectstarburst.engine.player

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/player")
@CrossOrigin(origins = arrayOf("*"))
class PlayerController {

    @Autowired lateinit var playerService: PlayerService

    @RequestMapping("/create")
    fun create(email: String, name: String): ResponseEntity<Player> {
        if (playerService.loadPlayerByEmail(email).isEmpty()){
            return ResponseEntity(playerService.createPlayer(email, name), HttpStatus.OK)
        }else {
            return ResponseEntity(Player(), HttpStatus.BAD_REQUEST)
        }
    }

    @RequestMapping("/loadById")
    fun loadById(playerId: String): ResponseEntity<Player> {

        val loadedPlayer: Player? = playerService.loadPlayerById(playerId)

        if (loadedPlayer != null) {
            return ResponseEntity(loadedPlayer, HttpStatus.OK)
        } else {
            return ResponseEntity(Player(), HttpStatus.BAD_REQUEST)
        }
    }

    @RequestMapping("/loadByEmail")
    fun loadByEmail(email: String): ResponseEntity<Player> {
        val loadedPlayers = playerService.loadPlayerByEmail(email)

        if (loadedPlayers.isEmpty()){
            return ResponseEntity(Player(), HttpStatus.BAD_REQUEST)
        }
        else {
            return ResponseEntity(loadedPlayers[0], HttpStatus.BAD_REQUEST) //TODO check if more than 1, which shouldnt happen
        }
    }
    @RequestMapping("/list")
    fun listAll(): ResponseEntity<List<Player>> {
        return ResponseEntity(playerService.listPlayers(), HttpStatus.OK)
    }
}