package com.spartancoder.projectstarburst.engine.player

import org.springframework.data.annotation.Id

class Player(
        @Id
        var id: String?=null,
        var email: String = "",
        var name: String = ""
)