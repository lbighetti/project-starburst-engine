package com.spartancoder.projectstarburst.engine.player

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PlayerService {

    @Autowired lateinit var playerRepository: PlayerRepository

    fun createPlayer (email:String, name:String): Player {
        return playerRepository.save(Player(email = email, name = name))
    }

    fun loadPlayerById(playerId:String): Player? {
        return playerRepository.findOne(playerId)
    }

    fun loadPlayerByEmail(email:String): List<Player> {
        return playerRepository.findByEmail(email)
    }

    fun listPlayers() : List<Player> {
        return playerRepository.findAll()
    }
}