package com.spartancoder.projectstarburst.engine

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.jsondoc.spring.boot.starter.EnableJSONDoc

@SpringBootApplication
@EnableJSONDoc
open class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}
