package com.spartancoder.projectstarburst.engine.character

import org.springframework.data.annotation.Id

class Character(
        @Id
        var id: String? = null,
        var name: String = "",
        var instrument: String = "",
        var playerId: String = ""
)