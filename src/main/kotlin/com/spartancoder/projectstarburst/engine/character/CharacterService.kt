package com.spartancoder.projectstarburst.engine.character

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CharacterService {
    @Autowired lateinit var charRepository: CharacterRepository

    fun createChar(name: String, instrument: String, playerId: String): Character {
        return charRepository.save(Character(name = name, instrument = instrument, playerId = playerId))
    }

    fun loadChar(charId:String): Character? {
        return charRepository.findOne(charId)
    }

    fun loadChars(playerId:String): List<Character> {
        return charRepository.findByPlayerId(playerId)
    }

    fun listChars() : List<Character> {
        return charRepository.findAll()
    }
}