package com.spartancoder.projectstarburst.engine.character

import com.spartancoder.projectstarburst.engine.player.Player
import com.spartancoder.projectstarburst.engine.player.PlayerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/char")
@CrossOrigin(origins = arrayOf("*"))
class CharacterController {

    @Autowired lateinit var charService: CharacterService
    @Autowired lateinit var playerService: PlayerService

    @RequestMapping("/create")
    fun create(name:String, instrument:String, playerId:String): ResponseEntity<Character> {
        val player: Player? = playerService.loadPlayerById(playerId)
        if (player != null){
            return ResponseEntity(charService.createChar(name, instrument, playerId), HttpStatus.OK)
        } else {
            return ResponseEntity(Character(), HttpStatus.OK) //FIXME Better way to handle not found
        }
    }

    @RequestMapping("/loadById")
    fun load(charId: String): ResponseEntity<Character> {

        val loadedChar: Character? = charService.loadChar(charId)

        if (loadedChar != null) {
            return ResponseEntity(loadedChar, HttpStatus.OK)
        } else {
            return ResponseEntity(Character(), HttpStatus.BAD_REQUEST) //FIXME Better way to handle not found
        }
    }

    @RequestMapping("/loadByPlayerId")
        fun loadByPlayerId(playerId: String): ResponseEntity<List<Character>> {
        val loadedChars: List<Character> = charService.loadChars(playerId)

        if (loadedChars.isEmpty()) {
            return ResponseEntity(ArrayList<Character>(), HttpStatus.BAD_REQUEST) //FIXME Better way to handle not found
        } else {
            return ResponseEntity(loadedChars, HttpStatus.OK)
        }
    }

    @RequestMapping("/list")
    fun listAll(): ResponseEntity<List<Character>> {
        return ResponseEntity(charService.listChars(), HttpStatus.OK)
    }
}