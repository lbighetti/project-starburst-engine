package com.spartancoder.projectstarburst.engine.character

import org.springframework.data.mongodb.repository.MongoRepository

interface CharacterRepository : MongoRepository<Character, String> {
    fun findByPlayerId(playerId: String): List<Character>
}